
from tkinter import *

from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
import sqlalchemy
import pandas as pd
from collections import Counter
from operator import itemgetter
import re


hasilR = "0"
hasilI = "0"
hasilA = "0"
hasilS = "0"
hasilE = "0"
hasilC = "0"
def showresultI_E(scoreIE):

    global skalaEks,skalaIn,Skala1

    if scoreIE < 0:

        skalaEks='0'
        skalaIn='introvert'
        Skala1 = 'Introvert'

    else:
        skalaEks = 'ekstrovert'
        skalaIn = '0'
        Skala1 = 'Ekstrovert'      
def showresult_S_N(scoreSN):

    global skalaSen,skalaInt,Skala2



    if scoreSN < 0:
        skalaSen = 'sensing'
        skalaInt = '0'
        Skala2 = 'Sensing'



    else:
        skalaInt = 'intuition'
        skalaSen = '0'
        Skala2 = 'Intuition'
def showresult_T_F(scoreTF):

    global skalaThi,skalaFeel,Skala3
    if scoreTF < 0:
        skalaThi = 'thinking'
        skalaFeel = '0'
        Skala3 = 'Thinking'
    else:
        skalaFeel = 'feeling'
        skalaThi = '0'
        Skala3 = 'Feeling'
def showresult_P_J(scorePJ):

    global skalaJud,skalaPer,Skala4

    Skala4=StringVar()

    if scorePJ <0:
        skalaPer =  'perception'
        skalaJud = '0'
        Skala4 = 'Perception'


    else:
        skalaJud =  'judgement'
        skalaPer = '0'
        Skala4 = 'Judgement'




    # btnnext=Button(root,text="isi data diri",command=).pack()
def showresultRIASEC(scoreR,scoreI,scoreA,scoreS,scoreE,scoreC):
    global listSkalaRIASEC, SkalaRIASEC

    global SkalaRIASEC
    global btndaftar

    SkalaRIASEC=StringVar()
    KategoriRIASEC = ['Realistic', 'Investigate', 'Artistic', 'Social', 'Enteprising', 'Conventional']

    scoreRIASEC = [scoreR, scoreI, scoreA, scoreS, scoreE, scoreC]

    print(scoreRIASEC)

    shortedRIASEC = sorted(zip(scoreRIASEC, KategoriRIASEC), reverse=True)[:3]

    listSkalaRIASEC = list(map(itemgetter(-1), shortedRIASEC))
    SkalaRIASEC = ' '.join(map(str, listSkalaRIASEC))
    listSkalaRIASEC = pd.Series(listSkalaRIASEC)
    print(SkalaRIASEC)
    print(type(SkalaRIASEC))

    HasilRiasec(hasilR, hasilI, hasilA, hasilS, hasilE, hasilC)
def HasilRiasec(hasilR,hasilI,hasilA,hasilS,hasilE,hasilC):
    for i in listSkalaRIASEC.index:
        if listSkalaRIASEC[i] == 'Realistic':
            hasilR = "realistic"
        elif listSkalaRIASEC[i] == 'Investigate':
            hasilI = "investigate"
        elif listSkalaRIASEC[i] == 'Artistic':
            hasilA = "artistic"
        elif listSkalaRIASEC[i] == 'Social':
            hasilS = "social"
        elif listSkalaRIASEC[i] == 'Enteprising':
            hasilE = "enteprising"
        elif listSkalaRIASEC[i] == 'Conventional':
            hasilC = "conventional"





df1 = pd.read_csv('data_ketentuan.csv', sep=';', error_bad_lines=False, index_col=False, dtype='unicode')

df1.loc[pd.to_numeric(df1['scale_e']) >0, 'skalaEks']  =    'ekstrovert'
df1.loc[pd.to_numeric(df1['scale_e']) ==0, 'skalaEks']  =    '0'
df1.loc[pd.to_numeric(df1['scale_i']) >0, 'skalaIn']   =    'introvert'
df1.loc[pd.to_numeric(df1['scale_i']) ==0, 'skalaIn']   =    '0'

df1.loc[pd.to_numeric(df1['scale_s']) >= 0, 'skalaSen']   =    'sensing'
df1.loc[pd.to_numeric(df1['scale_s']) == 0, 'skalaSen']   =    '0'
df1.loc[pd.to_numeric(df1['scale_n']) >= 0, 'skalaInt']   =    'intuition'
df1.loc[pd.to_numeric(df1['scale_n']) == 0, 'skalaInt']   =    '0'

df1.loc[pd.to_numeric(df1['scale_t']) >= 0, 'skalaThi']    =    'thinking'
df1.loc[pd.to_numeric(df1['scale_t']) == 0, 'skalaThi']    =    '0'
df1.loc[pd.to_numeric(df1['scale_f']) >= 0, 'skalaFeel']   =    'feeling'
df1.loc[pd.to_numeric(df1['scale_f']) == 0, 'skalaFeel']   =    '0'

df1.loc[pd.to_numeric(df1['scale_j']) >= 0, 'skalaJud']    =    'judgement'
df1.loc[pd.to_numeric(df1['scale_j']) == 0, 'skalaJud']    =    '0'
df1.loc[pd.to_numeric(df1['scale_p']) >= 0, 'skalaPer']    =    'perception'
df1.loc[pd.to_numeric(df1['scale_p']) == 0, 'skalaPer']    =    '0'



df1.loc[pd.to_numeric(df1['scaleRealistic'])>0 , 'skalaR'] = 'realistic'
df1.loc[pd.to_numeric(df1['scaleRealistic'])== 0, 'skalaR'] = '0'
df1.loc[pd.to_numeric(df1['scaleInvestigate'])>0, 'skalaI'] = 'investigate'
df1.loc[pd.to_numeric(df1['scaleInvestigate'])== 0, 'skalaI'] = '0'
df1.loc[pd.to_numeric(df1['scaleArtistic'])>0, 'skalaA'] = 'artistic'
df1.loc[pd.to_numeric(df1['scaleArtistic'])== 0, 'skalaA'] = '0'
df1.loc[pd.to_numeric(df1['scaleSocial'])>0, 'skalaS'] = 'social'
df1.loc[pd.to_numeric(df1['scaleSocial'])== 0, 'skalaS'] = '0'
df1.loc[pd.to_numeric(df1['scaleEnterpricing'])>0, 'skalaE'] = 'enteprising'
df1.loc[pd.to_numeric(df1['scaleEnterpricing'])== 0, 'skalaE'] = '0'
df1.loc[pd.to_numeric(df1['scaleConventional'])>0, 'skalaC'] = 'conventional'
df1.loc[pd.to_numeric(df1['scaleConventional'])== 0, 'skalaC'] = '0'



kebutuhanIntrovert      = skalaIn
kebutuhanEkstrovert     = skalaEks
kebutuhanSensing        = skalaSen
kebutuhanIntuition      = skalaInt
kebutuhanThinking       = skalaThi
kebutuhanFeeling        = skalaFeel
kebutuhanJudgement      = skalaJud
kebutuhanPerception     = skalaPer
kebutuhanRealistic      = skalaR
kebutuhanInvestigate    = skalaI
kebutuhanArtistic       = skalaA
kebutuhanSocial         = skalaS
kebutuhanEnterpricing   = skalaE
kebutuhanConventional   = skalaC

print("Ekstrovert",kebutuhanEkstrovert)
print("Introvert",kebutuhanIntrovert)
print("Sensing",kebutuhanSensing)
print("Intuition",kebutuhanIntuition)
print("Feeling",kebutuhanFeeling)
print("Judgement",kebutuhanJudgement)
print("Perception",kebutuhanPerception)
print("Realistic",kebutuhanRealistic)
print("Investigate",kebutuhanInvestigate)
print("Artistic",kebutuhanArtistic)
print("Social",kebutuhanSocial)
print("Enterpricing",kebutuhanEnterpricing)
print("Conventional",kebutuhanConventional)



stopworda = set(stopwords.words('english'))
clean_spcl = re.compile('[/(){}\[\]\|@,;]')
clean_symbol = re.compile('[^0-9a-z #+_]')


def clean_text(text):
    text = text.lower()  # lowercase text
    text = clean_spcl.sub(' ', text)
    text = clean_symbol.sub('', text)
    text = ' '.join(word for word in text.split() if word not in stopworda)
    return text




tf = TfidfVectorizer(analyzer='word', norm=None,use_idf=True,smooth_idf=True)
tfidf_matrixIntrovert       = tf.fit_transform(kebutuhanIntrovert)
tfidf_matrixEkstrovert      = tf.fit_transform(kebutuhanEkstrovert)
tfidf_matrixSensing         = tf.fit_transform(kebutuhanSensing)
tfidf_matrixIntuition       = tf.fit_transform(kebutuhanIntuition)
tfidf_matrixThinking        = tf.fit_transform(kebutuhanThinking)
tfidf_matrixFeeling         = tf.fit_transform(kebutuhanFeeling)
tfidf_matrixJudgement       = tf.fit_transform(kebutuhanJudgement)
tfidf_matrixPerception      = tf.fit_transform(kebutuhanPerception)
tfidf_matrixRealistic       = tf.fit_transform(kebutuhanRealistic)
tfidf_matrixInvestigate     = tf.fit_transform(kebutuhanInvestigate)
tfidf_matrixArtistic        = tf.fit_transform(kebutuhanArtistic)
tfidf_matrixSocial          = tf.fit_transform(kebutuhanSocial)
tfidf_matrixEnterpricing    = tf.fit_transform(kebutuhanEnterpricing)
tfidf_matrixConventional    = tf.fit_transform(kebutuhanConventional)


cos_simIntrovert    =cosine_similarity(tfidf_matrixIntrovert   ,tfidf_matrixIntrovert   )
cos_simEkstrovert   =cosine_similarity(tfidf_matrixEkstrovert  ,tfidf_matrixEkstrovert  )
cos_simSensing      =cosine_similarity(tfidf_matrixSensing     ,tfidf_matrixSensing     )
cos_simIntuition    =cosine_similarity(tfidf_matrixIntuition   ,tfidf_matrixIntuition   )
cos_simThinking     =cosine_similarity(tfidf_matrixThinking    ,tfidf_matrixThinking    )
cos_simFeeling      =cosine_similarity(tfidf_matrixFeeling     ,tfidf_matrixFeeling     )
cos_simJudgement    =cosine_similarity(tfidf_matrixJudgement   ,tfidf_matrixJudgement   )
cos_simPerception   =cosine_similarity(tfidf_matrixPerception  ,tfidf_matrixPerception  )
cos_simRealistic    =cosine_similarity(tfidf_matrixRealistic   ,tfidf_matrixRealistic   )
cos_simInvestigate  =cosine_similarity(tfidf_matrixInvestigate ,tfidf_matrixInvestigate )
cos_simArtistic     =cosine_similarity(tfidf_matrixArtistic    ,tfidf_matrixArtistic    )
cos_simSocial       =cosine_similarity(tfidf_matrixSocial      ,tfidf_matrixSocial      )
cos_simEnterpricing =cosine_similarity(tfidf_matrixEnterpricing,tfidf_matrixEnterpricing)
cos_simConventional =cosine_similarity(tfidf_matrixConventional,tfidf_matrixConventional)
print("cosim",cos_simIntrovert)
indicesIntrovert    = pd.Series(kebutuhanIntrovert   )
indicesEkstrovert   = pd.Series(kebutuhanEkstrovert  )
indicesSensing      = pd.Series(kebutuhanSensing     )
indicesIntuition    = pd.Series(kebutuhanIntuition   )
indicesThinking     = pd.Series(kebutuhanThinking    )
indicesFeeling      = pd.Series(kebutuhanFeeling     )
indicesJudgement    = pd.Series(kebutuhanJudgement   )
indicesPerception   = pd.Series(kebutuhanPerception  )
indicesRealistic    = pd.Series(kebutuhanRealistic   )
indicesInvestigate  = pd.Series(kebutuhanInvestigate )
indicesArtistic     = pd.Series(kebutuhanArtistic    )
indicesSocial       = pd.Series(kebutuhanSocial      )
indicesEnterpricing = pd.Series(kebutuhanEnterpricing)
indicesConventional = pd.Series(kebutuhanConventional)

indicesIntrovert   [:10]
indicesEkstrovert  [:10]
indicesSensing     [:10]
indicesIntuition   [:10]
indicesThinking    [:10]
indicesFeeling     [:10]
indicesJudgement   [:10]
indicesPerception  [:10]
indicesRealistic   [:10]
indicesInvestigate [:10]
indicesArtistic    [:10]
indicesSocial      [:10]
indicesEnterpricing[:10]
indicesConventional[:10]
print(kebutuhanIntrovert)
print (indicesIntrovert)

textIntrovert    = HasilIntrovert   
textEkstrovert   = HasilEkstrovert  
textSensing      = HasilSensing     
textIntuition    = HasilIntuition   
textThinking     = HasilThinking    
textFeeling      = HasilFeeling     
textJudgement    = HasilJudgement   
textPerception   = HasilPerception  
textRealistic    = HasilRealistic   
textInvestigate  = HasilInvestigate 
textArtistic     = HasilArtistic    
textSocial       = HasilSocial      
textEnterpricing = HasilEnterpricing
textConventional = HasilConventional

print("Hasil Ekstrovert",   textIntrovert   )
print("tipe Ekstrovert",   type(textIntrovert )  )

recommended_job = []

print(df_kerja.loc)
def recommendations(textIntrovert,textEkstrovert,textSensing,textIntuition,textThinking,textFeeling,textJudgement,textPerception,textRealistic,textInvestigate,textArtistic,textSocial,textEnterpricing,textConventional, cos_simIntrovert=cos_simIntrovert,cos_simEkstrovert=cos_simEkstrovert,cos_simSensing=cos_simSensing,cos_simIntuition=cos_simIntuition,cos_simThinking=cos_simThinking,cos_simFeeling=cos_simFeeling,cos_simJudgement=cos_simJudgement,cos_simPerception=cos_simPerception,cos_simRealistic=cos_simRealistic,cos_simInvestigate=cos_simInvestigate,cos_simArtistic=cos_simArtistic,cos_simSocial=cos_simSocial,cos_simEnterpricing=cos_simEnterpricing,cos_simConventional=cos_simConventional):
    print(textIntrovert)
    idxIntrovert = kebutuhanIntrovert[kebutuhanIntrovert == textIntrovert].index[0]
    idxEkstrovert = kebutuhanEkstrovert[kebutuhanEkstrovert == textEkstrovert].index[0]
    idxSensing = kebutuhanSensing[kebutuhanSensing == textSensing].index[0]
    idxIntuition = kebutuhanIntuition[kebutuhanIntuition == textIntuition].index[0]
    idxThinking = kebutuhanThinking[kebutuhanThinking == textThinking].index[0]
    idxFeeling = kebutuhanFeeling[kebutuhanFeeling == textFeeling].index[0]
    idxJudgement = kebutuhanJudgement[kebutuhanJudgement == textJudgement].index[0]
    idxPerception = kebutuhanPerception[kebutuhanPerception == textPerception].index[0]
    idxRealistic = kebutuhanRealistic[kebutuhanRealistic == textRealistic].index[0]
    idxInvestigate = kebutuhanInvestigate[kebutuhanInvestigate == textInvestigate].index[0]
    idxArtistic = kebutuhanArtistic[kebutuhanArtistic == textArtistic].index[0]
    idxSocial = kebutuhanSocial[kebutuhanSocial == textSocial].index[0]
    idxEnterpricing = kebutuhanEnterpricing[kebutuhanEnterpricing == textEnterpricing].index[0]
    idxConventional = kebutuhanConventional[kebutuhanConventional == textConventional].index[0]

    score_seriesIntrovert      = pd.Series(cos_simIntrovert    [idxIntrovert   ])      .sort_values(ascending=False)
    score_seriesEkstrovert     = pd.Series(cos_simEkstrovert   [idxEkstrovert  ])     .sort_values(ascending=False)
    score_seriesSensing        = pd.Series(cos_simSensing      [idxSensing     ])        .sort_values(ascending=False)
    score_seriesIntuition      = pd.Series(cos_simIntuition    [idxIntuition   ])      .sort_values(ascending=False)
    score_seriesThinking       = pd.Series(cos_simThinking     [idxThinking    ])        .sort_values(ascending=False)
    score_seriesFeeling        = pd.Series(cos_simFeeling      [idxFeeling     ])      .sort_values(ascending=False)
    score_seriesJudgement      = pd.Series(cos_simJudgement    [idxJudgement   ])     .sort_values(ascending=False)
    score_seriesPerception     = pd.Series(cos_simPerception   [idxPerception  ])      .sort_values(ascending=False)
    score_seriesRealistic      = pd.Series(cos_simRealistic    [idxRealistic   ])    .sort_values(ascending=False)
    score_seriesInvestigate    = pd.Series(cos_simInvestigate  [idxInvestigate ])       .sort_values(ascending=False)
    score_seriesArtistic       = pd.Series(cos_simArtistic     [idxArtistic    ])         .sort_values(ascending=False)
    score_seriesSocial         = pd.Series(cos_simSocial       [idxSocial      ])   .sort_values(ascending=False)
    score_seriesEnterpricing   = pd.Series(cos_simEnterpricing [idxEnterpricing])   .sort_values(ascending=False)
    score_seriesConventional   = pd.Series(cos_simConventional [idxConventional])   .sort_values(ascending=False)

    jobsIntrovert       = list(score_seriesIntrovert.iloc[0:2].index)
    jobsEkstrovert      = list(score_seriesEkstrovert.iloc[0:2].index)
    jobsSensing         = list(score_seriesSensing.iloc[0:2].index)
    jobsIntuition       = list(score_seriesIntuition.iloc[0:2].index)
    jobsThinking        = list(score_seriesThinking.iloc[0:2].index)
    jobsFeeling         = list(score_seriesFeeling.iloc[0:2].index)
    jobsJudgement       = list(score_seriesJudgement.iloc[0:2].index)
    jobsPerception      = list(score_seriesPerception.iloc[0:2].index)
    jobsRealistic       = list(score_seriesRealistic.iloc[0:2].index)
    jobsInvestigate     = list(score_seriesInvestigate.iloc[0:2].index)
    jobsArtistic        = list(score_seriesArtistic.iloc[0:2].index)
    jobsSocial          = list(score_seriesSocial.iloc[0:2].index)
    jobsEnterpricing    = list(score_seriesEnterpricing.iloc[0:2].index)
    jobsConventional    = list(score_seriesConventional.iloc[0:2].index)
    for i in jobsIntrovert:
        recommended_job.append(list(df1.index)[i])
    for i in jobsEkstrovert:
        recommended_job.append(list(df1.index)[i])
    for i in jobsSensing:
        recommended_job.append(list(df1.index)[i])
    for i in jobsIntuition:
        recommended_job.append(list(df1.index)[i])
    for i in jobsThinking:
        recommended_job.append(list(df1.index)[i])
    for i in jobsFeeling:
        recommended_job.append(list(df1.index)[i])
    for i in jobsJudgement:
        recommended_job.append(list(df1.index)[i])
    for i in jobsPerception:
        recommended_job.append(list(df1.index)[i])
    for i in jobsRealistic:
        recommended_job.append(list(df1.index)[i])
    for i in jobsInvestigate:
        recommended_job.append(list(df1.index)[i])
    for i in jobsArtistic:
        recommended_job.append(list(df1.index)[i])
    for i in jobsSocial:
        recommended_job.append(list(df1.index)[i])
    for i in jobsEnterpricing:
        recommended_job.append(list(df1.index)[i])
    for i in jobsConventional:
        recommended_job.append(list(df1.index)[i])
    print("Prediksi pekerjaan : ", recommended_job)
    rekomendasi = Counter(recommended_job).most_common(2)
    rekomendasi = list(map(lambda x: x[0], rekomendasi))

    print(rekomendasi)
    print(df1.jobtitle[rekomendasi])
    listOfjobs = df1.jobtitle[rekomendasi]

recommendations(textIntrovert,textEkstrovert,textSensing,textIntuition,textThinking,textFeeling,textJudgement,textPerception,textRealistic,textInvestigate,textArtistic,textSocial,textEnterpricing,textConventional)


