#!/usr/bin/env python3
from flask import Flask, render_template, send_from_directory, request
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
import sqlalchemy
import pandas as pd
from collections import Counter
from operator import itemgetter
import re
import json


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.db'
# @jsf.use(app)
@app.route('/')
def home():
    return render_template('index.html')

@app.route('/answer', methods=['GET'])
def serve_model():
    
    scoreIE = int(request.args.get('scoreIE'))
   
    print("ini score IE",scoreIE)
    print(type(scoreIE))
    scoreSN = int(request.args.get('scoreSN'))
    
    print("ini score SN",scoreSN)
    scoreTF = int(request.args.get('scoreTF'))
    
    print("ini score TF",scoreTF)
    scorePJ = int(request.args.get('scorePJ'))
  
    print("ini score PJ",scorePJ)
    scoreR  = int(request.args.get('scoreR'))
    print("ini score R",scoreR)
    scoreI  = int(request.args.get('scoreI'))
    print("ini score I",scoreI)
    scoreA  = int(request.args.get('scoreA'))
    print("ini score A",scoreA)
    scoreS  = int(request.args.get('scoreS'))
    print("ini score S",scoreS)
    scoreE  = int(request.args.get('scoreE'))
    print("ini score E",scoreE)
    scoreC  = int(request.args.get('scoreC'))
    print("ini score ",scoreC)
    
    showresult_I_E(scoreIE)
    showresult_S_N(scoreSN)
    showresult_T_F(scoreTF)
    showresult_P_J(scorePJ)
    showresultRIASEC(scoreR,scoreI,scoreA,scoreS,scoreE,scoreC)
    
    resultDanRecom = showresultRIASEC(scoreR,scoreI,scoreA,scoreS,scoreE,scoreC)
    riasec = resultDanRecom[1]
    recom = resultDanRecom[0]
    job1 = ""
    job2 = ""
    param = 1
    IE = showresult_I_E(scoreIE)
    SN = showresult_S_N(scoreSN)
    TF = showresult_T_F(scoreTF)
    PJ = showresult_P_J(scorePJ)
    print("ini test riasec",riasec[0])
    
    print("IE", IE)
    print("SN", SN)
    print("TF", TF)
    print("PJ", PJ)
    print("RiASEC1",riasec[0])
    print("RiASEC2",riasec[1])
    print("RiASEC3",riasec[2])
    print("hehe")
    for x in recom :
        if param == 1: 
            try:
                if x:
                    job1 = x
                    param = 0
                    print("iso")
            except IndexError:
                print("gak iso")
       
        else:
            try:
                if x:
                    job2 = x
                    print("iso")
            except IndexError:
                print("gak iso")
                    
    print("job1",job1)
    print("job2",job2)
    data = {'IE': IE,
            'SN': SN,
            'TF': TF,
            'PJ': PJ,
            'RIASEC1':riasec[0],
            'RIASEC2':riasec[1],
            'RIASEC3':riasec[2],
            'RecomJobs1': job1,
            'RecomJobs2': job2
            }
    print("ini data",data)
    return json.dumps(data)
    
   
def showresult_I_E(scoreIE):

    global skalaEks,skalaIn,Skala1

    if scoreIE < 0:

        skalaEks='0'
        skalaIn='introvert'
        Skala1 = 'Introvert'
        print(skalaIn)

    else:
        skalaEks = 'ekstrovert'
        skalaIn = '0'
        Skala1 = 'Ekstrovert'
        print(skalaEks)
    return Skala1
          
def showresult_S_N(scoreSN):
    global skalaSen,skalaInt,Skala2
    if scoreSN < 0:
        skalaSen = 'sensing'
        skalaInt = '0'
        Skala2 = 'Sensing'
    else:
        skalaInt = 'intuition'
        skalaSen = '0'
        Skala2 = 'Intuition'
    return Skala2
def showresult_T_F(scoreTF):
    global skalaThi,skalaFeel,Skala3
    if scoreTF < 0:
        skalaThi = 'thinking'
        skalaFeel = '0'
        Skala3 = 'Thinking'
    else:
        skalaFeel = 'feeling'
        skalaThi = '0'
        Skala3 = 'Feeling'
    return Skala3
def showresult_P_J(scorePJ):

    global skalaJud,skalaPer,Skala4
    if scorePJ <0:
        skalaPer =  'perception'
        skalaJud = '0'
        Skala4 = 'Perception'
    else:
        skalaJud =  'judgement'
        skalaPer = '0'
        Skala4 = 'Judgement'

    return Skala4


    # btnnext=Button(root,text="isi data diri",command=).pack()
def showresultRIASEC(scoreR,scoreI,scoreA,scoreS,scoreE,scoreC):

    hasilR = "0"
    hasilI = "0"
    hasilA = "0"
    hasilS = "0"
    hasilE = "0"
    hasilC = "0"
    global listSkalaRIASEC, SkalaRIASEC

    global SkalaRIASEC
    global btndaftar

    SkalaRIASEC=""
    KategoriRIASEC = ['Realistic', 'Investigate', 'Artistic', 'Social', 'Enteprising', 'Conventional']

    scoreRIASEC = [scoreR, scoreI, scoreA, scoreS, scoreE, scoreC]

    print(scoreRIASEC)

    shortedRIASEC = sorted(zip(scoreRIASEC, KategoriRIASEC), reverse=True)[:3]

    listSkalaRIASEC = list(map(itemgetter(-1), shortedRIASEC))
    SkalaRIASEC = ' '.join(map(str, listSkalaRIASEC))
    listSkalaRIASEC = pd.Series(listSkalaRIASEC)
    print(SkalaRIASEC)
    print(type(SkalaRIASEC))

    return HasilRiasec(hasilR, hasilI, hasilA, hasilS, hasilE, hasilC)


def HasilRiasec(hasilR,hasilI,hasilA,hasilS,hasilE,hasilC):
    for i in listSkalaRIASEC.index:
        if listSkalaRIASEC[i] == 'Realistic':
            hasilR = "realistic"
        elif listSkalaRIASEC[i] == 'Investigate':
            hasilI = "investigate"
        elif listSkalaRIASEC[i] == 'Artistic':
            hasilA = "artistic"
        elif listSkalaRIASEC[i] == 'Social':
            hasilS = "social"
        elif listSkalaRIASEC[i] == 'Enteprising':
            hasilE = "enteprising"
        elif listSkalaRIASEC[i] == 'Conventional':
            hasilC = "conventional"
    return preproses(skalaIn,skalaEks,skalaSen,skalaInt,skalaThi,skalaFeel,skalaJud,skalaPer,hasilR,hasilI,hasilA,hasilS,hasilE,hasilC),listSkalaRIASEC


df1 = pd.read_csv('data_ketentuan.csv', sep=';', error_bad_lines=False, index_col=False, dtype='unicode')

df1.loc[pd.to_numeric(df1['scale_e']) >0, 'skalaEks']  =    'ekstrovert'
df1.loc[pd.to_numeric(df1['scale_e']) ==0, 'skalaEks']  =    '0'
df1.loc[pd.to_numeric(df1['scale_i']) >0, 'skalaIn']   =    'introvert'
df1.loc[pd.to_numeric(df1['scale_i']) ==0, 'skalaIn']   =    '0'

df1.loc[pd.to_numeric(df1['scale_s']) >= 0, 'skalaSen']   =    'sensing'
df1.loc[pd.to_numeric(df1['scale_s']) == 0, 'skalaSen']   =    '0'
df1.loc[pd.to_numeric(df1['scale_n']) >= 0, 'skalaInt']   =    'intuition'
df1.loc[pd.to_numeric(df1['scale_n']) == 0, 'skalaInt']   =    '0'

df1.loc[pd.to_numeric(df1['scale_t']) >= 0, 'skalaThi']    =    'thinking'
df1.loc[pd.to_numeric(df1['scale_t']) == 0, 'skalaThi']    =    '0'
df1.loc[pd.to_numeric(df1['scale_f']) >= 0, 'skalaFeel']   =    'feeling'
df1.loc[pd.to_numeric(df1['scale_f']) == 0, 'skalaFeel']   =    '0'

df1.loc[pd.to_numeric(df1['scale_j']) >= 0, 'skalaJud']    =    'judgement'
df1.loc[pd.to_numeric(df1['scale_j']) == 0, 'skalaJud']    =    '0'
df1.loc[pd.to_numeric(df1['scale_p']) >= 0, 'skalaPer']    =    'perception'
df1.loc[pd.to_numeric(df1['scale_p']) == 0, 'skalaPer']    =    '0'



df1.loc[pd.to_numeric(df1['scaleRealistic'])>0 , 'skalaR'] = 'realistic'
df1.loc[pd.to_numeric(df1['scaleRealistic'])== 0, 'skalaR'] = '0'
df1.loc[pd.to_numeric(df1['scaleInvestigate'])>0, 'skalaI'] = 'investigate'
df1.loc[pd.to_numeric(df1['scaleInvestigate'])== 0, 'skalaI'] = '0'
df1.loc[pd.to_numeric(df1['scaleArtistic'])>0, 'skalaA'] = 'artistic'
df1.loc[pd.to_numeric(df1['scaleArtistic'])== 0, 'skalaA'] = '0'
df1.loc[pd.to_numeric(df1['scaleSocial'])>0, 'skalaS'] = 'social'
df1.loc[pd.to_numeric(df1['scaleSocial'])== 0, 'skalaS'] = '0'
df1.loc[pd.to_numeric(df1['scaleEnterpricing'])>0, 'skalaE'] = 'enteprising'
df1.loc[pd.to_numeric(df1['scaleEnterpricing'])== 0, 'skalaE'] = '0'
df1.loc[pd.to_numeric(df1['scaleConventional'])>0, 'skalaC'] = 'conventional'
df1.loc[pd.to_numeric(df1['scaleConventional'])== 0, 'skalaC'] = '0'

kebutuhanIntrovert      = df1["skalaIn"]
kebutuhanEkstrovert     = df1["skalaEks"]
kebutuhanSensing        = df1["skalaSen"]
kebutuhanIntuition      = df1["skalaInt"]
kebutuhanThinking       = df1["skalaThi"]
kebutuhanFeeling        = df1["skalaFeel"]
kebutuhanJudgement      = df1["skalaJud"]
kebutuhanPerception     = df1["skalaPer"]
kebutuhanRealistic      = df1["skalaR"]
kebutuhanInvestigate    = df1["skalaI"]
kebutuhanArtistic       = df1["skalaA"]
kebutuhanSocial         = df1["skalaS"]
kebutuhanEnterpricing   = df1["skalaE"]
kebutuhanConventional   = df1["skalaC"]

print("Ekstrovert",kebutuhanEkstrovert)
print("Introvert",kebutuhanIntrovert)
print("Sensing",kebutuhanSensing)
print("Intuition",kebutuhanIntuition)
print("Feeling",kebutuhanFeeling)
print("Judgement",kebutuhanJudgement)
print("Perception",kebutuhanPerception)
print("Realistic",kebutuhanRealistic)
print("Investigate",kebutuhanInvestigate)
print("Artistic",kebutuhanArtistic)
print("Social",kebutuhanSocial)
print("Enterpricing",kebutuhanEnterpricing)
print("Conventional",kebutuhanConventional)
tf = TfidfVectorizer(analyzer='word', norm=None,use_idf=True,smooth_idf=True)
tfidf_matrixIntrovert       = tf.fit_transform(kebutuhanIntrovert)
tfidf_matrixEkstrovert      = tf.fit_transform(kebutuhanEkstrovert)
tfidf_matrixSensing         = tf.fit_transform(kebutuhanSensing)
tfidf_matrixIntuition       = tf.fit_transform(kebutuhanIntuition)
tfidf_matrixThinking        = tf.fit_transform(kebutuhanThinking)
tfidf_matrixFeeling         = tf.fit_transform(kebutuhanFeeling)
tfidf_matrixJudgement       = tf.fit_transform(kebutuhanJudgement)
tfidf_matrixPerception      = tf.fit_transform(kebutuhanPerception)
tfidf_matrixRealistic       = tf.fit_transform(kebutuhanRealistic)
tfidf_matrixInvestigate     = tf.fit_transform(kebutuhanInvestigate)
tfidf_matrixArtistic        = tf.fit_transform(kebutuhanArtistic)
tfidf_matrixSocial          = tf.fit_transform(kebutuhanSocial)
tfidf_matrixEnterpricing    = tf.fit_transform(kebutuhanEnterpricing)
tfidf_matrixConventional    = tf.fit_transform(kebutuhanConventional)


cos_simIntrovert    =cosine_similarity(tfidf_matrixIntrovert   ,tfidf_matrixIntrovert   )
cos_simEkstrovert   =cosine_similarity(tfidf_matrixEkstrovert  ,tfidf_matrixEkstrovert  )
cos_simSensing      =cosine_similarity(tfidf_matrixSensing     ,tfidf_matrixSensing     )
cos_simIntuition    =cosine_similarity(tfidf_matrixIntuition   ,tfidf_matrixIntuition   )
cos_simThinking     =cosine_similarity(tfidf_matrixThinking    ,tfidf_matrixThinking    )
cos_simFeeling      =cosine_similarity(tfidf_matrixFeeling     ,tfidf_matrixFeeling     )
cos_simJudgement    =cosine_similarity(tfidf_matrixJudgement   ,tfidf_matrixJudgement   )
cos_simPerception   =cosine_similarity(tfidf_matrixPerception  ,tfidf_matrixPerception  )
cos_simRealistic    =cosine_similarity(tfidf_matrixRealistic   ,tfidf_matrixRealistic   )
cos_simInvestigate  =cosine_similarity(tfidf_matrixInvestigate ,tfidf_matrixInvestigate )
cos_simArtistic     =cosine_similarity(tfidf_matrixArtistic    ,tfidf_matrixArtistic    )
cos_simSocial       =cosine_similarity(tfidf_matrixSocial      ,tfidf_matrixSocial      )
cos_simEnterpricing =cosine_similarity(tfidf_matrixEnterpricing,tfidf_matrixEnterpricing)
cos_simConventional =cosine_similarity(tfidf_matrixConventional,tfidf_matrixConventional)

indicesIntrovert    = pd.Series(kebutuhanIntrovert   )
indicesEkstrovert   = pd.Series(kebutuhanEkstrovert  )
indicesSensing      = pd.Series(kebutuhanSensing     )
indicesIntuition    = pd.Series(kebutuhanIntuition   )
indicesThinking     = pd.Series(kebutuhanThinking    )
indicesFeeling      = pd.Series(kebutuhanFeeling     )
indicesJudgement    = pd.Series(kebutuhanJudgement   )
indicesPerception   = pd.Series(kebutuhanPerception  )
indicesRealistic    = pd.Series(kebutuhanRealistic   )
indicesInvestigate  = pd.Series(kebutuhanInvestigate )
indicesArtistic     = pd.Series(kebutuhanArtistic    )
indicesSocial       = pd.Series(kebutuhanSocial      )
indicesEnterpricing = pd.Series(kebutuhanEnterpricing)
indicesConventional = pd.Series(kebutuhanConventional)

indicesIntrovert   [:10]
indicesEkstrovert  [:10]
indicesSensing     [:10]
indicesIntuition   [:10]
indicesThinking    [:10]
indicesFeeling     [:10]
indicesJudgement   [:10]
indicesPerception  [:10]
indicesRealistic   [:10]
indicesInvestigate [:10]
indicesArtistic    [:10]
indicesSocial      [:10]
indicesEnterpricing[:10]
indicesConventional[:10]
print(kebutuhanIntrovert)
print (indicesIntrovert)




stopworda = set(stopwords.words('english'))
clean_spcl = re.compile('[/(){}\[\]\|@,;]')
clean_symbol = re.compile('[^0-9a-z #+_]')

def clean_text(text):
    text = text.lower()  # lowercase text
    text = clean_spcl.sub(' ', text)
    text = clean_symbol.sub('', text)
    text = ' '.join(word for word in text.split() if word not in stopworda)
    return text

def preproses(skalaIn,skalaEks,skalaSen,skalaInt,skalaThi,skalaFeel,skalaJud,skalaPer,hasilR,hasilI,hasilA,hasilS,hasilE,hasilC):
    HasilIntrovert        = skalaIn
    HasilEkstrovert       = skalaEks
    HasilSensing          = skalaSen
    HasilIntuition        = skalaInt
    HasilThinking         = skalaThi
    HasilFeeling          = skalaFeel
    HasilJudgement        = skalaJud
    HasilPerception       = skalaPer
    HasilRealistic        = hasilR
    HasilInvestigate      = hasilI
    HasilArtistic         = hasilA
    HasilSocial           = hasilS
    HasilEnterpricing     = hasilE
    HasilConventional     = hasilC
    
    textIntrovert    = HasilIntrovert   
    textEkstrovert   = HasilEkstrovert  
    textSensing      = HasilSensing     
    textIntuition    = HasilIntuition   
    textThinking     = HasilThinking    
    textFeeling      = HasilFeeling     
    textJudgement    = HasilJudgement   
    textPerception   = HasilPerception  
    textRealistic    = HasilRealistic   
    textInvestigate  = HasilInvestigate 
    textArtistic     = HasilArtistic    
    textSocial       = HasilSocial      
    textEnterpricing = HasilEnterpricing
    textConventional = HasilConventional

    print("Hasil Ekstrovert",   textIntrovert   )
    print("tipe Ekstrovert",   type(textIntrovert )  )
    return recommendations(textIntrovert,textEkstrovert,textSensing,textIntuition,textThinking,textFeeling,textJudgement,textPerception,textRealistic,textInvestigate,textArtistic,textSocial,textEnterpricing,textConventional)

def recommendations(textIntrovert,textEkstrovert,textSensing,textIntuition,textThinking,textFeeling,textJudgement,textPerception,textRealistic,textInvestigate,textArtistic,textSocial,textEnterpricing,textConventional, cos_simIntrovert=cos_simIntrovert,cos_simEkstrovert=cos_simEkstrovert,cos_simSensing=cos_simSensing,cos_simIntuition=cos_simIntuition,cos_simThinking=cos_simThinking,cos_simFeeling=cos_simFeeling,cos_simJudgement=cos_simJudgement,cos_simPerception=cos_simPerception,cos_simRealistic=cos_simRealistic,cos_simInvestigate=cos_simInvestigate,cos_simArtistic=cos_simArtistic,cos_simSocial=cos_simSocial,cos_simEnterpricing=cos_simEnterpricing,cos_simConventional=cos_simConventional):
    print(textIntrovert)
    recommended_job = []
    idxIntrovert = kebutuhanIntrovert[kebutuhanIntrovert == textIntrovert].index[0]
    idxEkstrovert = kebutuhanEkstrovert[kebutuhanEkstrovert == textEkstrovert].index[0]
    idxSensing = kebutuhanSensing[kebutuhanSensing == textSensing].index[0]
    idxIntuition = kebutuhanIntuition[kebutuhanIntuition == textIntuition].index[0]
    idxThinking = kebutuhanThinking[kebutuhanThinking == textThinking].index[0]
    idxFeeling = kebutuhanFeeling[kebutuhanFeeling == textFeeling].index[0]
    idxJudgement = kebutuhanJudgement[kebutuhanJudgement == textJudgement].index[0]
    idxPerception = kebutuhanPerception[kebutuhanPerception == textPerception].index[0]
    idxRealistic = kebutuhanRealistic[kebutuhanRealistic == textRealistic].index[0]
    idxInvestigate = kebutuhanInvestigate[kebutuhanInvestigate == textInvestigate].index[0]
    idxArtistic = kebutuhanArtistic[kebutuhanArtistic == textArtistic].index[0]
    idxSocial = kebutuhanSocial[kebutuhanSocial == textSocial].index[0]
    idxEnterpricing = kebutuhanEnterpricing[kebutuhanEnterpricing == textEnterpricing].index[0]
    idxConventional = kebutuhanConventional[kebutuhanConventional == textConventional].index[0]

    counterIntrovert = 0
    counterEkstrovert = 0
    counterSensing = 0
    counterIntuition = 0
    counterThinking = 0
    counterFeeling = 0
    counterJud = 0
    counterPer = 0
    counterRealistic = 0
    counterInves = 0
    counterArt = 0
    counterSocial = 0
    counterEnterprice = 0
    counterConventional = 0

    arryIntrovert = []
    arryEkstrovert = []
    arrySensing = []
    arryIntuition = []
    arryThinking = []
    arryFeeling = []
    arryJud = []
    arryPer = []
    arryRealistic = []
    arryInves = []
    arryArt = []
    arrySocial = []
    arryEnterprice = []
    arryConventional = []
    score_seriesIntrovert      = pd.Series(cos_simIntrovert    [idxIntrovert   ])
    for i in score_seriesIntrovert:
        if i == 1.0:
            print(i)
            arryIntrovert.append(counterIntrovert)
        counterIntrovert += 1
    print("Introvert",arryIntrovert)
    print("score introvert",score_seriesIntrovert)
    score_seriesEkstrovert     = pd.Series(cos_simEkstrovert   [idxEkstrovert  ])
    for i in score_seriesEkstrovert:
        if i == 1.0:
            print(i)
            arryEkstrovert.append(counterEkstrovert)
        counterEkstrovert += 1
    print("Ekstrovert",arryEkstrovert)
    print("score eks", score_seriesEkstrovert)
    score_seriesSensing        = pd.Series(cos_simSensing      [idxSensing     ])
    for i in score_seriesSensing:
        if i == 1.0:
            print(i)
            arrySensing.append(counterSensing)
        counterSensing += 1
    print("sensing",arrySensing)
    print("score sen", score_seriesSensing)
    score_seriesIntuition      = pd.Series(cos_simIntuition    [idxIntuition   ])
    for i in score_seriesIntuition:
        if i == 1.0:
            print(i)
            arryIntuition.append(counterIntuition)
        counterIntuition += 1
    print("Intuition",arryIntuition)
    print("score sen", score_seriesIntuition)
    score_seriesThinking       = pd.Series(cos_simThinking     [idxThinking    ])
    for i in score_seriesThinking:
        if i == 1.0:
            print(i)
            arryThinking.append(counterThinking)
        counterThinking += 1
    print("think",arryThinking)
    print("score Think", score_seriesThinking)
    score_seriesFeeling        = pd.Series(cos_simFeeling      [idxFeeling     ])
    for i in score_seriesFeeling:
        if i == 1.0:
            print(i)
            arryFeeling.append(counterFeeling)
        counterThinking += 1
    print("feel", arryFeeling)
    print("score feel", score_seriesFeeling)
    score_seriesJudgement      = pd.Series(cos_simJudgement    [idxJudgement   ])
    for i in score_seriesJudgement:
        if i == 1.0:
            print(i)
            arryJud.append(counterJud)
        counterThinking += 1
    print("jud", arryJud)
    print("score Jud", score_seriesJudgement)
    score_seriesPerception     = pd.Series(cos_simPerception   [idxPerception  ])
    for i in score_seriesPerception:
        if i == 1.0:
            print(i)
            arryPer.append(counterPer)
        counterPer += 1
    print("Per", arryPer)
    print("score Per", score_seriesPerception)
    score_seriesRealistic      = pd.Series(cos_simRealistic    [idxRealistic   ])
    for i in score_seriesRealistic:
        if i == 1.0:
            print(i)
            arryRealistic.append(counterRealistic)
        counterRealistic += 1
    print("Real", arryRealistic)
    print("score Real", score_seriesRealistic)
    score_seriesInvestigate    = pd.Series(cos_simInvestigate  [idxInvestigate ])
    for i in score_seriesInvestigate:
        if i == 1.0:
            print(i)
            arryInves.append(counterInves)
        counterInves += 1
    print("Inves", arryInves)
    print("score Inves", score_seriesInvestigate)
    score_seriesArtistic       = pd.Series(cos_simArtistic     [idxArtistic    ])
    for i in score_seriesArtistic:
        if i == 1.0:
            print(i)
            arryArt.append(counterArt)
        counterArt += 1
    print("Art", arryArt)
    print("score Art", score_seriesArtistic)
    score_seriesSocial         = pd.Series(cos_simSocial       [idxSocial      ])
    for i in score_seriesSocial:
        if i == 1.0:
            print(i)
            arrySocial.append(counterSocial)
        counterSocial += 1
    print("Social", arrySocial)
    print("score social", score_seriesSocial)
    score_seriesEnterpricing   = pd.Series(cos_simEnterpricing [idxEnterpricing])
    for i in score_seriesEnterpricing:
        if i == 1.0:
            print(i)
            arryEnterprice.append(counterEnterprice)
        counterEnterprice += 1
    print("Enterprice", arryEnterprice)
    print("score Enterprice", score_seriesEnterpricing)
    score_seriesConventional   = pd.Series(cos_simConventional [idxConventional])
    for i in score_seriesConventional:
        if i == 1.0:
            print(i)
            arryConventional.append(counterConventional)
        counterConventional += 1
    print("Conventional", arryConventional)
    print("score Conventional", score_seriesConventional)

    for i in arryIntrovert:
        recommended_job.append(list(df1.index)[i])
        print("Recom introvert",recommended_job)
    for i in arryEkstrovert:
        recommended_job.append(list(df1.index)[i])
        print("Recom Ekstrovert", recommended_job)
    for i in arrySensing:
        recommended_job.append(list(df1.index)[i])
        print("Recom Sensing", recommended_job)
    for i in arryIntuition:
        recommended_job.append(list(df1.index)[i])
        print("Recom Intuition", recommended_job)
    for i in arryThinking:
        recommended_job.append(list(df1.index)[i])
        print("Recom Think", recommended_job)
    for i in arryFeeling:
        recommended_job.append(list(df1.index)[i])
        print("Recom Feeling", recommended_job)
    for i in arryJud:
        recommended_job.append(list(df1.index)[i])
        print("Recom Judge", recommended_job)
    for i in arryPer:
        recommended_job.append(list(df1.index)[i])
        print("Recom per", recommended_job)
    for i in arryRealistic:
        recommended_job.append(list(df1.index)[i])
        print("Recom Realistic", recommended_job)
    for i in arryInves:
        recommended_job.append(list(df1.index)[i])
        print("Recom Inves", recommended_job)
    for i in arryArt:
        recommended_job.append(list(df1.index)[i])
        print("Recom Art", recommended_job)
    for i in arrySocial:
        recommended_job.append(list(df1.index)[i])
        print("Recom Social", recommended_job)
    for i in arryEnterprice:
        recommended_job.append(list(df1.index)[i])
        print("Recom Enter", recommended_job)
    for i in arryConventional:
        recommended_job.append(list(df1.index)[i])
        print("Recom Conven", recommended_job)
    print("Prediksi pekerjaan : ", recommended_job)
    rekomendasi = Counter(recommended_job).most_common(2)
    rekomendasi = list(map(lambda x: x[0], rekomendasi))

    print(rekomendasi)
    print(df1.jobtitle[rekomendasi])
    listOfjobs = df1.jobtitle[rekomendasi]
    
    print(listOfjobs)
    return listOfjobs

@app.route('/public/<path:path>')
def public(path):
    return send_from_directory('public', path)

if __name__ == '__main__':
        app.run(debug=True, port=33000)
    # app.run(host='109.106.255.235')
    # localhost:5000
